@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('empresa_pt', 'Empresa [PT]') !!}
    {!! Form::textarea('empresa_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textos']) !!}
</div>

<div class="form-group">
    {!! Form::label('empresa_en', 'Empresa [EN]') !!}
    {!! Form::textarea('empresa_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textos']) !!}
</div>

<div class="form-group">
    {!! Form::label('empresa_cn', 'Empresa [CN]') !!}
    {!! Form::textarea('empresa_cn', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textos']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('quem_somos_pt', 'Quem Somos [PT]') !!}
    {!! Form::textarea('quem_somos_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textos']) !!}
</div>

<div class="form-group">
    {!! Form::label('quem_somos_en', 'Quem Somos [EN]') !!}
    {!! Form::textarea('quem_somos_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textos']) !!}
</div>

<div class="form-group">
    {!! Form::label('quem_somos_cn', 'Quem Somos [CN]') !!}
    {!! Form::textarea('quem_somos_cn', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textos']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('fundos_pt', 'Fundos [PT]') !!}
    {!! Form::textarea('fundos_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textos']) !!}
</div>

<div class="form-group">
    {!! Form::label('fundos_en', 'Fundos [EN]') !!}
    {!! Form::textarea('fundos_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textos']) !!}
</div>

<div class="form-group">
    {!! Form::label('fundos_cn', 'Fundos [CN]') !!}
    {!! Form::textarea('fundos_cn', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textos']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
