<?php

use Illuminate\Database\Seeder;

class MainCoinsSeeder extends Seeder
{
    public function run()
    {
        DB::table('main_coins')->insert([
            'subtitulo_pt' => '',
            'subtitulo_en' => '',
            'subtitulo_cn' => '',
            'titulo_pt' => '',
            'titulo_en' => '',
            'titulo_cn' => '',
            'texto_pt' => '',
            'texto_en' => '',
            'texto_cn' => '',
            'valor' => '',
            'pagamento_pt' => '',
            'pagamento_en' => '',
            'pagamento_cn' => '',
        ]);
    }
}
