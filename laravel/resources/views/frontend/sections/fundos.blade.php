    <section class="fundos">
        <div class="center">
            <div class="textos">
                <div class="titulo">
                    <h2>{{ trans('frontend.fundos.titulo') }}</h2>
                </div>
                <div class="texto">
                    {!! $textos->{Tools::trans('fundos')} !!}
                </div>
            </div>

            @foreach($fundos as $fundo)
                <div class="fundo">
                    <div class="titulo">
                        <img src="{{ asset('assets/img/fundos/'.$fundo->marca) }}" alt="">
                        <p>
                            <strong>{{ $fundo->titulo }}</strong> -
                            {!! $fundo->{Tools::trans('texto')} !!}
                        </p>
                        <a href="{{ route('contato', ['origem' => $fundo->titulo]) }}" class="lightbox-content">{{ trans('frontend.fundos.registre') }}</a>
                    </div>
                    <div class="informacoes">
                        @if(count($fundo->grafico))
                        <div class="grafico" @if(!count($fundo->performance) && !count($fundo->taxas)) style="margin-bottom:0" @endif>
                            @include('frontend._grafico', compact('fundo'))
                        </div>
                        @else
                        <div class="breve" @if(!count($fundo->performance) && !count($fundo->taxas)) style="margin-bottom:0" @endif>
                            <span>{{ trans('frontend.fundos.breve') }}</span>
                        </div>
                        @endif

                        <div class="tabelas">
                            @if(count($fundo->performance))
                            <table class="performance">
                                <thead>
                                    <th colspan="4">{{ trans('frontend.fundos.performance') }}</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><strong>{{ trans('frontend.fundos.periodo') }}</strong></td>
                                        <td><strong>{{ trans('frontend.fundos.retorno') }}</strong></td>
                                        <td><strong>{{ trans('frontend.fundos.volatilidade') }}</strong></td>
                                        <td><strong>{{ trans('frontend.fundos.perda') }}</strong></td>
                                    </tr>
                                    @foreach($fundo->performance as $p)
                                    <tr>
                                        <td>{{ $p->{Tools::trans('periodo')} }}</td>
                                        <td>{{ $p->retorno }}</td>
                                        <td>{{ $p->volatilidade }}</td>
                                        <td>{{ $p->perda_maxima }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                            @if(count($fundo->taxas))
                            <table class="taxas">
                                <thead>
                                    <th colspan="2">{{ trans('frontend.fundos.taxas') }}</th>
                                </thead>
                                <tbody>
                                    @foreach($fundo->taxas as $t)
                                    <tr>
                                        <td>{{ $t->{Tools::trans('nome')} }}</td>
                                        <td>{{ $t->valor }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
