@include('painel.common.flash')

<div class="alert alert-info">
    Utilize somente números, ponto e vírgula.
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('quantidade', 'Quantidade') !!}
    {!! Form::text('quantidade', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('valor', 'Valor') !!}
    {!! Form::text('valor', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('porcentagem', 'Porcentagem') !!}
    {!! Form::text('porcentagem', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.moedas.index') }}" class="btn btn-default btn-voltar">Voltar</a>
