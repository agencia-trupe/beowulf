<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxasTable extends Migration
{
    public function up()
    {
        Schema::create('taxas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fundo_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('nome_pt');
            $table->string('nome_en');
            $table->string('nome_cn');
            $table->string('valor');
            $table->timestamps();
            $table->foreign('fundo_id')->references('id')->on('fundos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('taxas');
    }
}
