@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Moedas /</small> Editar Moeda</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.moedas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.moedas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
