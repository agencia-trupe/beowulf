@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome_pt', 'Nome [PT]') !!}
    {!! Form::text('nome_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome_en', 'Nome [EN]') !!}
    {!! Form::text('nome_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome_cn', 'Nome [CN]') !!}
    {!! Form::text('nome_cn', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('valor', 'Valor') !!}
    {!! Form::text('valor', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.fundos.taxas.index', $fundo) }}" class="btn btn-default btn-voltar">Voltar</a>
