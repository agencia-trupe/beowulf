@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Moedas /</small> Adicionar Moeda</h2>
    </legend>

    {!! Form::open(['route' => 'painel.moedas.store', 'files' => true]) !!}

        @include('painel.moedas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
