    <div class="lightbox contato">
        <h2>{{ trans('frontend.fundos.registre') }}</h2>
        <h3>{{ request('origem') }}</h3>

        <form action="" id="form-contato-lightbox" method="POST">
            <input type="hidden" name="origem" id="l_origem" value="{{ request('origem') }}">
            <input type="text" name="nome" id="l_nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
            <input type="email" name="email" id="l_email" placeholder="{{ trans('frontend.contato.email') }}" required>
            <div id="form-contato-lightbox-response" class="response"></div>
            <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
        </form>
    </div>
