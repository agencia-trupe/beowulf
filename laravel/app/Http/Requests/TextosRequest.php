<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TextosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'empresa_pt' => 'required',
            'empresa_en' => 'required',
            'empresa_cn' => 'required',
            'quem_somos_pt' => 'required',
            'quem_somos_en' => 'required',
            'quem_somos_cn' => 'required',
            'fundos_pt' => 'required',
            'fundos_en' => 'required',
            'fundos_cn' => 'required',
        ];
    }
}
