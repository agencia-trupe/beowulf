@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Fundos /</small> Adicionar Fundo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.fundos.store', 'files' => true]) !!}

        @include('painel.fundos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
