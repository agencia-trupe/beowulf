<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MainCoinsRequest;
use App\Http\Controllers\Controller;

use App\Models\MainCoins;

class MainCoinsController extends Controller
{
    public function index()
    {
        $registro = MainCoins::first();

        return view('painel.main-coins.edit', compact('registro'));
    }

    public function update(MainCoinsRequest $request, MainCoins $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.main-coins.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
