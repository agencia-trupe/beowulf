<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MoedasRequest;
use App\Http\Controllers\Controller;

use App\Models\Moeda;

class MoedasController extends Controller
{
    public function index()
    {
        $registros = Moeda::ordenados()->get();

        return view('painel.moedas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.moedas.create');
    }

    public function store(MoedasRequest $request)
    {
        try {

            $input = $request->all();

            Moeda::create($input);

            return redirect()->route('painel.moedas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Moeda $registro)
    {
        return view('painel.moedas.edit', compact('registro'));
    }

    public function update(MoedasRequest $request, Moeda $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.moedas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Moeda $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.moedas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
