<ul class="nav navbar-nav">
	<li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
		<a href="{{ route('painel.banners.index') }}">Banners</a>
	</li>
    <li @if(Tools::routeIs('painel.moedas*')) class="active" @endif>
        <a href="{{ route('painel.moedas.index') }}">Moedas</a>
    </li>
    <li @if(Tools::routeIs('painel.textos*')) class="active" @endif>
        <a href="{{ route('painel.textos.index') }}">Textos</a>
    </li>
    <li @if(Tools::routeIs('painel.equipe*')) class="active" @endif>
        <a href="{{ route('painel.equipe.index') }}">Equipe</a>
    </li>
    <li @if(Tools::routeIs('painel.fundos*')) class="active" @endif>
		<a href="{{ route('painel.fundos.index') }}">Fundos</a>
	</li>
    <li @if(Tools::routeIs('painel.main-coins*')) class="active" @endif>
		<a href="{{ route('painel.main-coins.index') }}">Main Coins</a>
	</li>
    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>
    <li @if(Tools::routeIs('painel.newsletter*')) class="active" @endif>
        <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
    </li>
</ul>
