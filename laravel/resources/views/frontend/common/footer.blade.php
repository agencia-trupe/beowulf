    <footer>
        <div class="center">
            <p>
                Copyright © {{ date('Y') }} {{ config('app.name') }}
                <span>|</span>
                <a href="http://www.trupe.net" target="_blank">{{ trans('frontend.footer.sites') }}</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
