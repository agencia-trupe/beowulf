<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\NewsletterRequest;

use App\Models\Banner;
use App\Models\Moeda;
use App\Models\Textos;
use App\Models\Membro;
use App\Models\Fundo;
use App\Models\MainCoins;
use App\Models\Newsletter;
use App\Models\Contato;

class HomeController extends Controller
{
    public function index()
    {
        $contato   = Contato::first();
        $banners   = Banner::ordenados()->get();
        $moedas    = Moeda::ordenados()->get();
        $textos    = Textos::first();
        $equipe    = Membro::ordenados()->get();
        $fundos    = Fundo::ordenados()->get();
        $mainCoins = MainCoins::first();

        return view('frontend.home', compact(
            'contato',
            'banners',
            'moedas',
            'textos',
            'equipe',
            'fundos',
            'mainCoins'
        ));
    }

    public function newsletter(NewsletterRequest $request)
    {
        Newsletter::create($request->all());

        return response()->json([
            'message' => trans('frontend.newsletter.sucesso')
        ]);
    }
}
