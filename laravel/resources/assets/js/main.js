import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('nav a').click(function(event) {
    event.preventDefault();

    var section = $(this).data('section');

    $('html, body').animate({
        scrollTop: $('section.'+section).offset().top
    });

    if ($(this).parent().parent().is('#nav-mobile')) {
        $('#mobile-toggle').trigger('click');
    }
});

$('.banners').cycle({
    slides: '.slide'
});

$('#form-newsletter').submit(function(event) {
    event.preventDefault();

    var $form = $(this);

    if ($form.hasClass('sending')) return false;

    $form.addClass('sending');

    $.ajax({
        type: 'POST',
        url: $form.attr('action'),
        data: {
            email: $form.find('input[name=newsletter_email]').val(),
        }
    }).done(function(data) {
        alert(data.message);
        $form[0].reset();
    }).fail(function(data) {
        var res = data.responseJSON,
            txt = res.email;
        alert(txt);
    }).always(function() {
        $form.removeClass('sending');
    });
});

$('#form-contato').on('submit', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-contato-response');

    $response.fadeOut('fast');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/contato',
        data: {
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            mensagem: $('#mensagem').val(),
        },
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
        },
        error: function(data) {
            if (data.responseJSON) {
                var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                $response.fadeOut().text(error).fadeIn('slow');
            }
        },
        dataType: 'json'
    });
});

$('.lightbox-content').fancybox({
    helpers: {
        overlay: {
            locked: false,
            css: {'background-color': 'rgba(0,0,0,.5)'},
        }
    },
    type: 'ajax',
    maxWidth: 980,
    maxHeight: '95%',
    padding: 0
});

$(document).on('submit', '#form-contato-lightbox', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-contato-lightbox-response');

    $response.fadeOut('fast');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/contato-lightbox',
        data: {
            origem: $('#l_origem').val(),
            nome: $('#l_nome').val(),
            email: $('#l_email').val(),
            telefone: $('#l_telefone').val(),
            mensagem: $('#l_mensagem').val(),
        },
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
            $.fancybox.update();
        },
        error: function(data) {
            if (data.responseJSON) {
                var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                $response.fadeOut().text(error).fadeIn('slow');
                $.fancybox.update();
            }
        },
        dataType: 'json'
    });
});
