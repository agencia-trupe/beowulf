<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewsletterRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email|unique:newsletter,email'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => trans('frontend.newsletter.required'),
            'email.email'    => trans('frontend.newsletter.valid'),
            'email.unique'   => trans('frontend.newsletter.unique')
        ];
    }
}
