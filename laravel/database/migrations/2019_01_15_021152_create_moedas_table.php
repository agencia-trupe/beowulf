<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoedasTable extends Migration
{
    public function up()
    {
        Schema::create('moedas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('quantidade');
            $table->string('valor');
            $table->string('porcentagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('moedas');
    }
}
