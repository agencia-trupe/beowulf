    <section class="maincoins">
        <div class="center">
            <h3>{{ $mainCoins->{Tools::trans('subtitulo')} }}</h3>
            <h2>{{ $mainCoins->{Tools::trans('titulo')} }}</h2>

            <div class="box">
                <div class="texto">
                    <img src="{{ asset('assets/img/layout/maincoins.png') }}" alt="">
                    <p>{!! $mainCoins->{Tools::trans('texto')} !!}</p>
                </div>
                <div class="valor">
                    <a href="{{ route('contato', ['origem' => 'Main Coins']) }}" class="lightbox-content">{{ trans('frontend.fundos.registre') }}</a>
                    <table>
                        <thead>
                            <th colspan="2">
                                {{ trans('frontend.fundos.preco') }}
                            </th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $mainCoins->valor }}</td>
                                <td>{{ $mainCoins->{Tools::trans('pagamento')} }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
