<?php

namespace App\Helpers;

class Tools
{
    public static function routeIs($routeNames)
    {
        foreach ((array) $routeNames as $routeName) {
            if (str_is($routeName, \Route::currentRouteName())) {
                return true;
            }
        }

        return false;
    }

    public static function fileUpload($input, $path = 'arquivos/')
    {
        if (! $file = request()->file($input)) {
            throw new \Exception("O campo (${input}) não contém nenhum arquivo.", 1);
        }

        $fileName  = str_slug(
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)
        );
        $fileName .= '_'.date('YmdHis');
        $fileName .= str_random(10);
        $fileName .= '.'.$file->getClientOriginalExtension();

        $file->move(public_path($path), $fileName);

        return $fileName;
    }

    public static function parseLink($url)
    {
        return parse_url($url, PHP_URL_SCHEME) === null ? 'http://'.$url : $url;
    }

    public static function meses()
    {
        return [
            'pt' => [
                1  => 'JAN',
                2  => 'FEV',
                3  => 'MAR',
                4  => 'ABR',
                5  => 'MAI',
                6  => 'JUN',
                7  => 'JUL',
                8  => 'AGO',
                9  => 'SET',
                10 => 'OUT',
                11 => 'NOV',
                12 => 'DEZ'
            ],
            'en' => [
                1  => 'JAN',
                2  => 'FEB',
                3  => 'MAR',
                4  => 'APR',
                5  => 'MAY',
                6  => 'JUN',
                7  => 'JUL',
                8  => 'AUG',
                9  => 'SEP',
                10 => 'OCT',
                11 => 'NOV',
                12 => 'DEC'
            ],
            'cn' => [
                1  => '一月',
                2  => '二月',
                3  => '三月',
                4  => '四月',
                5  => '五月',
                6  => '六月',
                7  => '七月',
                8  => '八月',
                9  => '九月',
                10 => '十月',
                11 => '十一月',
                12 => '十二月'
            ]
        ];
    }

    public static function trans($termo)
    {
        return $termo.'_'.app()->getLocale();
    }

    public static function isEmptyRow($row)
    {
        foreach($row as $cell) {
            if (null !== $cell) return false;
        }
        return true;
    }
}
