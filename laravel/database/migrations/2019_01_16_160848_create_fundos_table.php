<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFundosTable extends Migration
{
    public function up()
    {
        Schema::create('fundos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('marca');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_cn');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('fundos');
    }
}
