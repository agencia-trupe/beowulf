    <section class="quem-somos">
        <div class="center">
            <div class="textos">
                <div class="titulo">
                    <h2>{{ trans('frontend.quem-somos') }}</h2>
                </div>
                <div class="texto">
                    {!! $textos->{Tools::trans('quem_somos')} !!}
                </div>
            </div>
        </div>

        <div class="equipe">
            <div class="center">
                @foreach($equipe as $membro)
                <div class="membro">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/equipe/'.$membro->foto) }}" alt="">
                        <div class="social">
                            @foreach(['facebook', 'twitter', 'linkedin', 'instagram'] as $s)
                                @if($membro->{$s})
                                <a href="{{ Tools::parseLink($membro->{$s}) }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="texto">
                        <h3>{{ $membro->nome }}</h3>
                        <p>{!! $membro->{Tools::trans('texto')} !!}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
