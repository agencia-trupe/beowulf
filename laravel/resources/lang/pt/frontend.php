<?php

return [

    'newsletter' => [
        'newsletter' => 'Newsletter',
        'chamada'    => 'Atualize-se com as tendências mundiais. Assine a nossa newsletter mensal.',
        'email'      => 'e-mail',
        'required'   => 'Informe seu e-mail',
        'valid'      => 'Informe um endereço de e-mail válido.',
        'unique'     => 'Este e-mail já está registrado.',
        'sucesso'    => 'Agradecemos seu cadastro!'
    ],

    'empresa'    => 'Empresa',
    'quem-somos' => 'Quem somos',
    'pesquisa'   => 'Pesquisa',

    'fundos' => [
        'titulo'       => 'Fundos',
        'registre'     => 'Cadastre-se',
        'preco'        => 'Preço',
        'performance'  => 'Performance',
        'periodo'      => 'Período',
        'retorno'      => 'Retorno',
        'volatilidade' => 'Volatilidade',
        'perda'        => 'Rebaixamento máximo',
        'taxas'        => 'Honorários',
        'breve'        => 'em breve',
    ],

    'contato' => [
        'titulo'   => 'Contate-nos',
        'frase'    => 'Nossa política de atendimento preza sermos acessíveis. Não hesite em nos contatar!',
        'nome'     => 'nome',
        'email'    => 'e-mail',
        'telefone' => 'telefone',
        'mensagem' => 'mensagem',
        'enviar'   => 'enviar',
        'erro'     => 'Por favor preencha os campos corretamente.',
        'enviado'  => 'Sua mensagem foi enviada!'
    ],

    'footer' => [
        'sites' => 'Criação de sites:'
    ],

];
