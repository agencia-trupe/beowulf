@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Fundos / {{ $fundo->titulo }} / Gráfico /</small> Editar Ponto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.fundos.grafico.update', $fundo, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.fundos.grafico.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
