<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PerformanceRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'periodo_pt' => 'required',
            'periodo_en' => 'required',
            'periodo_cn' => 'required',
            'retorno' => 'required',
            'volatilidade' => 'required',
            'perda_maxima' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
