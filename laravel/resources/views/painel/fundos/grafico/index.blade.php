@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.fundos.index') }}" title="Voltar para Fundos" class="btn btn-sm btn-default">
        &larr; Voltar para Fundos
    </a>

    <legend>
        <h2>
            <small>Fundos / {{ $fundo->titulo }} /</small> Gráfico
        </h2>
    </legend>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Importar Arquivo</h4>
        </div>
        <div class="panel-body">
            {!! Form::open(['route' => ['painel.fundos.grafico.importar', $fundo->id], 'files' => true]) !!}
                <div class="input-group">
                    {!! Form::file('arquivo', ['class' => 'form-control', 'required' => true]) !!}
                    <span class="input-group-btn">
                        {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
                    </span>
                </div>
            {!! Form::close() !!}

            <div class="alert alert-info" style="margin:10px 0 0">
                <span class="glyphicon glyphicon-info-sign" style="margin-right: 10px;font-size:1.1em"></span>
                Ao importar um novo arquivo todos os registros abaixo serão substituídos.
            </div>
        </div>
        <div class="panel-footer">
            <div class="btn-group btn-block">
                <a href="{{ route('painel.fundos.grafico.limpar', $fundo->id) }}" class="btn btn-danger btn-md">
                    <span class="glyphicon glyphicon-trash" style="margin-right:5px"></span>
                    Limpar Gráfico
                </a>
                <a href="{{ route('painel.fundos.grafico.modelo', $fundo->id) }}" class="btn btn-warning btn-md">
                    <span class="glyphicon glyphicon-download" style="margin-right:5px"></span>
                    Baixar Modelo
                </a>
            </div>
        </div>
    </div>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Data</th>
                <th>Valor</th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    @if($registro->mes && $registro->ano)
                        {{ Tools::meses()['pt'][$registro->mes] }} {{ $registro->ano }}
                    @endif
                </td>
                <td>{{ $registro->valor }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
