<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TaxasRequest;
use App\Http\Controllers\Controller;

use App\Models\Taxa;
use App\Models\Fundo;

class TaxasController extends Controller
{
    public function index(Fundo $fundo)
    {
        $registros = $fundo->taxas()->ordenados()->get();

        return view('painel.fundos.taxas.index', compact('fundo', 'registros'));
    }

    public function create(Fundo $fundo)
    {
        return view('painel.fundos.taxas.create', compact('fundo'));
    }

    public function store(Fundo $fundo, TaxasRequest $request)
    {
        try {

            $input = $request->all();

            $fundo->taxas()->create($input);

            return redirect()->route('painel.fundos.taxas.index', $fundo)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Fundo $fundo, Taxa $registro)
    {
        return view('painel.fundos.taxas.edit', compact('fundo', 'registro'));
    }

    public function update(Fundo $fundo, TaxasRequest $request, Taxa $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.fundos.taxas.index', $fundo)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Fundo $fundo, Taxa $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.fundos.taxas.index', $fundo)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
