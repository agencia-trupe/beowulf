@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('foto', 'Foto') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/equipe/'.$registro->foto) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('foto', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('texto_pt', 'Texto [PT]') !!}
    {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_en', 'Texto [EN]') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_cn', 'Texto [CN]') !!}
    {!! Form::textarea('texto_cn', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('facebook', 'Facebook') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('twitter', 'Twitter') !!}
    {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('linkedin', 'LinkedIn') !!}
    {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.equipe.index') }}" class="btn btn-default btn-voltar">Voltar</a>
