<?php

use Illuminate\Database\Seeder;

class TextosSeeder extends Seeder
{
    public function run()
    {
        DB::table('textos')->insert([
            'empresa_pt' => '',
            'empresa_en' => '',
            'empresa_cn' => '',
            'quem_somos_pt' => '',
            'quem_somos_en' => '',
            'quem_somos_cn' => '',
            'fundos_pt' => '',
            'fundos_en' => '',
            'fundos_cn' => '',
        ]);
    }
}
