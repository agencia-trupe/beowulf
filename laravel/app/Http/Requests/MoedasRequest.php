<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MoedasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'quantidade' => 'required|regex:/^-?[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            'valor' => 'required|regex:/^-?[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
            'porcentagem' => 'required|regex:/^-?[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)*$/',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
