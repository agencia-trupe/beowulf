@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('twitter', 'Twitter') !!}
    {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('frase_pt', 'Frase [PT]') !!}
    {!! Form::text('frase_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_en', 'Frase [EN]') !!}
    {!! Form::text('frase_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_cn', 'Frase [CN]') !!}
    {!! Form::text('frase_cn', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
