    <header>
        <div class="center">
            <div class="logo">{{ config('app.name') }}</div>
            <div class="icones">
                <div class="social">
                    @foreach(['instagram', 'twitter'] as $s)
                        @if($contato->{$s})
                        <a href="{{ Tools::parseLink($contato->{$s}) }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                        @endif
                    @endforeach
                </div>
                <div class="lang">
                    @foreach(['en', 'pt', 'cn'] as $l)
                        @if(app()->getLocale() !== $l)
                        <a href="{{ route('lang', $l) }}" class="lang-{{ $l }}" title="{{ strtoupper($l) }}">{{ strtoupper($l) }}</a>
                        @endif
                    @endforeach
                </div>
            </div>
            <nav id="nav-desktop">
                @include('frontend.common.nav')
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
    </header>

    <nav id="nav-mobile">
        <div class="center">
            @include('frontend.common.nav')
        </div>
    </nav>
