<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TaxasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome_pt' => 'required',
            'nome_en' => 'required',
            'nome_cn' => 'required',
            'valor' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
