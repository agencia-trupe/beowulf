<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EquipeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'foto' => 'required|image',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_cn' => 'required',
            'facebook' => '',
            'twitter' => '',
            'linkedin' => '',
            'instagram' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['foto'] = 'image';
        }

        return $rules;
    }
}
