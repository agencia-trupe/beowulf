<?php

return [

    'newsletter' => [
        'newsletter' => 'Newsletter',
        'chamada'    => 'Stay tuned for the world\'s changes! Receive our Monthly Newsletter.',
        'email'      => 'e-mail',
        'required'   => 'Insert your e-mail.',
        'valid'      => 'Insert a valid e-mail address.',
        'unique'     => 'This e-mail is already registered.',
        'sucesso'    => 'Thank you for registering!'
    ],

    'empresa'    => 'Company',
    'quem-somos' => 'Who We Are',
    'pesquisa'   => 'Research',

    'fundos' => [
        'titulo'       => 'Funds',
        'registre'     => 'Register Now',
        'preco'        => 'Price',
        'performance'  => 'Performance',
        'periodo'      => 'Period',
        'retorno'      => 'Return',
        'volatilidade' => 'Volatility',
        'perda'        => 'Max drawdown',
        'taxas'        => 'Fees',
        'breve'        => 'coming soon',
    ],

    'contato' => [
        'titulo'   => 'Contact Us',
        'frase'    => 'Being accessible is part of our culture! Do not hesitate to contact us! We\'ll be glad to answer you!',
        'nome'     => 'name',
        'email'    => 'e-mail',
        'telefone' => 'voice',
        'mensagem' => 'message',
        'enviar'   => 'Send',
        'erro'     => 'Please fill out all fields correctly.',
        'enviado'  => 'Message sent successfully!'
    ],

    'footer' => [
        'sites' => 'Sites creation'
    ],

];
