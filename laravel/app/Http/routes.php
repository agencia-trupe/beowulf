<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::post('contato-lightbox', 'ContatoController@postLightbox')->name('contato.postLightbox');

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if (in_array($idioma, ['pt', 'en', 'cn'])) {
            Session::put('locale', $idioma);
        }
        return redirect()->route('home');
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('fundos', 'FundosController');
		Route::resource('fundos.taxas', 'TaxasController');
        Route::resource('fundos.performance', 'PerformanceController');
        Route::post('fundos/{fundos}/grafico/importar', 'GraficoController@importar')->name('painel.fundos.grafico.importar');
        Route::get('fundos/{fundos}/grafico/limpar', 'GraficoController@limpar')->name('painel.fundos.grafico.limpar');
        Route::get('fundos/{fundos}/grafico/modelo', 'GraficoController@modelo')->name('painel.fundos.grafico.modelo');
		Route::resource('fundos.grafico', 'GraficoController', ['only' => ['index']]);
		Route::resource('main-coins', 'MainCoinsController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
		Route::resource('equipe', 'EquipeController');
		Route::resource('textos', 'TextosController', ['only' => ['index', 'update']]);
		Route::resource('moedas', 'MoedasController');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::resource('newsletter', 'NewsletterController', [
            'only' => ['index', 'destroy']
        ]);
        Route::get('newsletter/exportar', 'NewsletterController@exportar')->name('painel.newsletter.exportar');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
