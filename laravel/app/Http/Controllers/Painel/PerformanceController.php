<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PerformanceRequest;
use App\Http\Controllers\Controller;

use App\Models\Periodo;
use App\Models\Fundo;

class PerformanceController extends Controller
{
    public function index(Fundo $fundo)
    {
        $registros = $fundo->performance()->ordenados()->get();

        return view('painel.fundos.performance.index', compact('fundo', 'registros'));
    }

    public function create(Fundo $fundo)
    {
        return view('painel.fundos.performance.create', compact('fundo'));
    }

    public function store(Fundo $fundo, PerformanceRequest $request)
    {
        try {

            $input = $request->all();

            $fundo->performance()->create($input);

            return redirect()->route('painel.fundos.performance.index', $fundo)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Fundo $fundo, Periodo $registro)
    {
        return view('painel.fundos.performance.edit', compact('fundo', 'registro'));
    }

    public function update(Fundo $fundo, PerformanceRequest $request, Periodo $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.fundos.performance.index', $fundo)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Fundo $fundo, Periodo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.fundos.performance.index', $fundo)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
