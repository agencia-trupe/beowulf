    <section class="contato">
        <div class="center">
            <h2>{{ trans('frontend.contato.titulo') }}</h2>
            <p>{{ $contato->{Tools::trans('frase')} }}</p>

            <form action="" id="form-contato" method="POST">
                <input type="text" name="nome" id="nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                <input type="email" name="email" id="email" placeholder="{{ trans('frontend.contato.email') }}" required>
                <input type="text" name="mensagem" id="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required>
                <div id="form-contato-response" class="response"></div>
                <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
            </form>
        </div>
    </section>
