@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Fundos /</small> Editar Fundo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.fundos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.fundos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
