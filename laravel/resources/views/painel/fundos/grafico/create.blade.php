@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Fundos / {{ $fundo->titulo }} / Gráfico /</small> Adicionar Ponto</h2>
    </legend>

    {!! Form::open(['route' => ['painel.fundos.grafico.store', $fundo], 'files' => true]) !!}

        @include('painel.fundos.grafico.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
