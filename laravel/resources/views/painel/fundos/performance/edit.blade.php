@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Fundos / {{ $fundo->titulo }} / Performance /</small> Editar Período</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.fundos.performance.update', $fundo, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.fundos.performance.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
