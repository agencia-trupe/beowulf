<canvas id="grafico-{{ $fundo->id }}"></canvas>
<script>
(function() {
    var xGridLinesColors = ['#fff', 'transparent'];
    var rowsCount = {{ count($fundo->graficoData()['data']) }};
    for(var i = 0; i < rowsCount; i++) {
        xGridLinesColors.push(xGridLinesColors[1]);
    }

    var ctx = document.getElementById('grafico-{{ $fundo->id }}');
    var grafico = new Chart(ctx, {
        type: 'line',
        data: {
            labels: {!! json_encode(array_merge([''], $fundo->graficoData()['labels'], [''])) !!},
            datasets: [{
                data: {!! json_encode(array_merge([null], $fundo->graficoData()['data'], [null])) !!},
                fill: false,
                borderColor: '#fff',
                borderWidth: 2,
                pointBackgroundColor: '#fff',
                pointRadius: 1,
                pointHoverRadius: 1
            }],
        },
        options: {
            title: { display: true },
            maintainAspectRatio: false,
            legend: { display: false },
            tooltips: { enabled: false },
            elements: { line: { tension: 0, } },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: 'white',
                        fontSize: 10,
                        padding: 10,
                        beginAtZero: false,
                        callback: function (label, index, labels) {
                            if (index === 0 || index === labels.length -1) {
                                return label;
                            } else { return ''; }
                        }
                    },
                    gridLines: {
                        zeroLineColor: '#fff',
                        drawBorder: false,
                        color: '#2E7656'
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: 'white',
                        fontSize: 10,
                        padding: 10,
                        autoSkip: false,
                        maxRotation: 0
                    },
                    gridLines: {
                        color: xGridLinesColors
                    }
                }]
            },
        }
    });
})();
</script>
