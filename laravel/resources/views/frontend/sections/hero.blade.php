    <div class="banners">
        @foreach($banners as $banner)
        <div class="slide" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})"></div>
        @endforeach
    </div>

    <section class="hero">
        <div class="center">
            <div class="newsletter">
                <p>{{ trans('frontend.newsletter.chamada') }}</p>
                <form action="{{ route('newsletter') }}" id="form-newsletter" method="POST">
                    <input type="email" name="newsletter_email" placeholder="{{ trans('frontend.newsletter.email') }}" required>
                    <input type="submit" value="{{ trans('frontend.newsletter.newsletter') }}!">
                </form>
            </div>
            <div class="moedas">
                @foreach($moedas as $moeda)
                    {{ $moeda->nome }}
                    <span class="{{ $moeda->quantidade >= 0 ? 'positivo' : 'negativo' }}">{{ $moeda->quantidade }}</span>
                    ${{ $moeda->valor }}
                    <span class="{{ $moeda->porcentagem >= 0 ? 'positivo' : 'negativo' }}">{{ $moeda->porcentagem }}%</span>
                    <span class="divider">|</span>
                @endforeach
            </div>
        </div>
    </section>
