@extends('frontend.common.template')

@section('content')

    @include('frontend.sections.hero')
    @include('frontend.sections.empresa')
    @include('frontend.sections.quem-somos')
    @include('frontend.sections.fundos')
    @include('frontend.sections.maincoins')
    @include('frontend.sections.contato')

@endsection
