<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextosTable extends Migration
{
    public function up()
    {
        Schema::create('textos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('empresa_pt');
            $table->text('empresa_en');
            $table->text('empresa_cn');
            $table->text('quem_somos_pt');
            $table->text('quem_somos_en');
            $table->text('quem_somos_cn');
            $table->text('fundos_pt');
            $table->text('fundos_en');
            $table->text('fundos_cn');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('textos');
    }
}
