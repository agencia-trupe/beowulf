    <section class="empresa">
        <div class="center">
            <div class="textos">
                <div class="titulo">
                    <h2>{{ trans('frontend.empresa') }}</h2>
                </div>
                <div class="texto">
                    {!! $textos->{Tools::trans('empresa')} !!}
                </div>
            </div>
        </div>
    </section>
