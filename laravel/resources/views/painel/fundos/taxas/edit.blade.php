@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Fundos / {{ $fundo->titulo }} / Taxas /</small> Editar Taxa</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.fundos.taxas.update', $fundo, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.fundos.taxas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
