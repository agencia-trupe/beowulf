<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MainCoinsRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'subtitulo_pt' => 'required',
            'subtitulo_en' => 'required',
            'subtitulo_cn' => 'required',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_cn' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_cn' => 'required',
            'valor' => 'required',
            'pagamento_pt' => 'required',
            'pagamento_en' => 'required',
            'pagamento_cn' => 'required',
        ];
    }
}
