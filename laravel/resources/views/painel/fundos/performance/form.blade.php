@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('periodo_pt', 'Período [PT]') !!}
    {!! Form::text('periodo_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('periodo_en', 'Período [EN]') !!}
    {!! Form::text('periodo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('periodo_cn', 'Período [CN]') !!}
    {!! Form::text('periodo_cn', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('retorno', 'Retorno') !!}
    {!! Form::text('retorno', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('volatilidade', 'Volatilidade') !!}
    {!! Form::text('volatilidade', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('perda_maxima', 'Perda máxima') !!}
    {!! Form::text('perda_maxima', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.fundos.performance.index', $fundo) }}" class="btn btn-default btn-voltar">Voltar</a>
