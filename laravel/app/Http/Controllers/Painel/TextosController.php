<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TextosRequest;
use App\Http\Controllers\Controller;

use App\Models\Textos;

class TextosController extends Controller
{
    public function index()
    {
        $registro = Textos::first();

        return view('painel.textos.edit', compact('registro'));
    }

    public function update(TextosRequest $request, Textos $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.textos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
