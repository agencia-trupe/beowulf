<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FundosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'marca' => 'required|image',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_cn' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['marca'] = 'image';
        }

        return $rules;
    }
}
