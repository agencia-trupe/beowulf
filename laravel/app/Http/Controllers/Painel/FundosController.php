<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FundosRequest;
use App\Http\Controllers\Controller;

use App\Models\Fundo;

class FundosController extends Controller
{
    public function index()
    {
        $registros = Fundo::ordenados()->get();

        return view('painel.fundos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.fundos.create');
    }

    public function store(FundosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Fundo::upload_marca();

            Fundo::create($input);

            return redirect()->route('painel.fundos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Fundo $registro)
    {
        return view('painel.fundos.edit', compact('registro'));
    }

    public function update(FundosRequest $request, Fundo $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Fundo::upload_marca();

            $registro->update($input);

            return redirect()->route('painel.fundos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Fundo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.fundos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
