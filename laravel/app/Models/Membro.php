<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Membro extends Model
{
    protected $table = 'equipe';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_foto()
    {
        return CropImage::make('foto', [
            'width'  => 250,
            'height' => 250,
            'path'   => 'assets/img/equipe/'
        ]);
    }
}
