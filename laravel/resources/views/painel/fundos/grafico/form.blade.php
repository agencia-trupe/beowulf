@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('mes', 'Mês') !!}
    {!! Form::select('mes', Tools::meses()['pt'], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('ano', 'Ano') !!}
    {!! Form::text('ano', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('valor', 'Valor (0-100)') !!}
    {!! Form::text('valor', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.fundos.grafico.index', $fundo) }}" class="btn btn-default btn-voltar">Voltar</a>
