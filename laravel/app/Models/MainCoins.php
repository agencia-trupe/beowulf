<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class MainCoins extends Model
{
    protected $table = 'main_coins';

    protected $guarded = ['id'];

}
