<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraficoTable extends Migration
{
    public function up()
    {
        Schema::create('grafico', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fundo_id')->unsigned()->nullable();
            $table->integer('mes');
            $table->integer('ano');
            $table->string('valor');
            $table->timestamps();
            $table->foreign('fundo_id')->references('id')->on('fundos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('grafico');
    }
}
