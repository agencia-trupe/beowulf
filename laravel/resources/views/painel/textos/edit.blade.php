@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Textos</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.textos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.textos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
