<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;

use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\Fundo;

use Illuminate\Support\Facades\Mail;

class ContatoController extends Controller
{
    public function index()
    {
        if (! $this->verificaOrigem(request('origem'))) {
            abort('500');
        }

        return view('frontend.contato');
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $data = $request->all();
        $data['origem'] = 'Contato';

        $contatoRecebido->create($data);
        $this->sendMail($data);

        $response = [
            'message' => trans('frontend.contato.enviado')
        ];

        return response()->json($response);
    }

    public function postLightbox(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        if (! $this->verificaOrigem(request('origem'))) {
            abort('500');
        }

        $data = $request->all();

        $contatoRecebido->create($data);
        $this->sendMail($data);

        $response = [
            'message' => trans('frontend.contato.enviado')
        ];

        return response()->json($response);
    }

    private function verificaOrigem($origem)
    {
        return
            $origem == 'Main Coins' ||
            Fundo::where('titulo', $origem)->count();
    }

    private function sendMail($data)
    {
        if (! $email = Contato::first()->email) {
           return false;
        }

        Mail::send('emails.contato', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('app.name'))
               ->subject('[CONTATO] '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
