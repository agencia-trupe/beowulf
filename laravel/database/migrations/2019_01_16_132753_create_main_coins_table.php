<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainCoinsTable extends Migration
{
    public function up()
    {
        Schema::create('main_coins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subtitulo_pt');
            $table->string('subtitulo_en');
            $table->string('subtitulo_cn');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_cn');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_cn');
            $table->string('valor');
            $table->string('pagamento_pt');
            $table->string('pagamento_en');
            $table->string('pagamento_cn');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('main_coins');
    }
}
