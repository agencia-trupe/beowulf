<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GraficoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'mes' => 'required_with:ano|integer|between:1,12',
            'ano' => 'required_with:mes|integer|digits:4',
            'valor' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
