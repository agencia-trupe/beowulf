@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Main Coins</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.main-coins.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.main-coins.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
