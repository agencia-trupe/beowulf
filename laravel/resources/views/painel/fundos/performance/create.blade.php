@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Fundos / {{ $fundo->titulo }} / Performance /</small> Adicionar Período</h2>
    </legend>

    {!! Form::open(['route' => ['painel.fundos.performance.store', $fundo], 'files' => true]) !!}

        @include('painel.fundos.performance.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
