@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Fundos / {{ $fundo->titulo }} / Taxas /</small> Adicionar Taxa</h2>
    </legend>

    {!! Form::open(['route' => ['painel.fundos.taxas.store', $fundo], 'files' => true]) !!}

        @include('painel.fundos.taxas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
