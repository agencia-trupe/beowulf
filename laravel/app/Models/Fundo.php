<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use App\Models\Taxa;
use App\Models\Periodo;
use App\Models\Ponto;
use App\Helpers\Tools;

class Fundo extends Model
{
    protected $table = 'fundos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_marca()
    {
        return CropImage::make('marca', [
            'width'  => 180,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/fundos/'
        ]);
    }

    public function taxas()
    {
        return $this->hasMany(Taxa::class, 'fundo_id');
    }

    public function performance()
    {
        return $this->hasMany(Periodo::class, 'fundo_id');
    }

    public function grafico()
    {
        return $this->hasMany(Ponto::class, 'fundo_id');
    }

    public function graficoData()
    {
        return [
            'labels' => $this->grafico()->ordenados()->get()->map(
                function($p) {
                    return ($p->mes && $p->ano)
                        ? [Tools::meses()[app()->getLocale()][$p->mes], $p->ano]
                        : '';
                })->toArray(),
            'data' => array_map(function ($v) {
                return str_replace(',', '.', $v);
            }, $this->grafico()->ordenados()->pluck('valor')->toArray())
        ];
    }
}
