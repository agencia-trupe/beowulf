<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Ponto extends Model
{
    protected $table = 'grafico';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('id', 'ASC');
    }
}
