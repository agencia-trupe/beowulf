<?php

return [

    'newsletter' => [
        'newsletter' => '通讯',
        'chamada'    => '请继续关注世界的变化！接收我们的月刊。',
        'email'      => '电子邮件',
        'required'   => '输入您的电子邮件。',
        'valid'      => '输入有效的电子邮件。',
        'unique'     => '此邮箱号已被注册。',
        'sucesso'    => '感谢您注册！'
    ],

    'empresa'    => '公司',
    'quem-somos' => '公司介绍',
    'pesquisa'   => '研究',

    'fundos' => [
        'titulo'       => '资金',
        'registre'     => '立即注册',
        'preco'        => '价钱',
        'performance'  => '性能',
        'periodo'      => '时期',
        'retorno'      => '返回',
        'volatilidade' => '挥发性',
        'perda'        => '最大亏损',
        'taxas'        => '费用',
        'breve'        => '快来了',
    ],

    'contato' => [
        'titulo'   => '联系我们',
        'frase'    => '无障碍是我们的文化！ 不要犹豫与我们联系！ 我们很乐意回复您！',
        'nome'     => '姓名',
        'email'    => '电子邮件',
        'telefone' => '语音',
        'mensagem' => '信息',
        'enviar'   => '发送',
        'erro'     => '请正确填写所有字段。',
        'enviado'  => '信息已成功发送！'
    ],

    'footer' => [
        'sites' => '网站创建'
    ],

];
