<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformanceTable extends Migration
{
    public function up()
    {
        Schema::create('performance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fundo_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('periodo_pt');
            $table->string('periodo_en');
            $table->string('periodo_cn');
            $table->string('retorno');
            $table->string('volatilidade');
            $table->string('perda_maxima');
            $table->timestamps();
            $table->foreign('fundo_id')->references('id')->on('fundos')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('performance');
    }
}
