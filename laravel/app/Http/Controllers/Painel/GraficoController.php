<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GraficoRequest;
use App\Http\Controllers\Controller;

use App\Models\Ponto;
use App\Models\Fundo;
use App\Helpers\Tools;

use Maatwebsite\Excel\Facades\Excel;

class GraficoController extends Controller
{
    public function index(Fundo $fundo)
    {
        $registros = $fundo->grafico()->ordenados()->get();

        return view('painel.fundos.grafico.index', compact('fundo', 'registros'));
    }

    public function create(Fundo $fundo)
    {
        return view('painel.fundos.grafico.create', compact('fundo'));
    }

    public function store(Fundo $fundo, GraficoRequest $request)
    {
        try {
            $input = $request->all();

            $fundo->grafico()->create($input);

            return redirect()->route('painel.fundos.grafico.index', $fundo)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Fundo $fundo, Ponto $registro)
    {
        return view('painel.fundos.grafico.edit', compact('fundo', 'registro'));
    }

    public function update(Fundo $fundo, GraficoRequest $request, Ponto $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.fundos.grafico.index', $fundo)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Fundo $fundo, Ponto $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.fundos.grafico.index', $fundo)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function importar(Fundo $fundo, Request $request)
    {
        if (! $request->hasFile('arquivo') ||
            ! in_array(
                $request->arquivo->getClientOriginalExtension(),
                ['xls', 'xlsx']
            )
        ) {
            return back()->withErrors(['Erro ao importar produtos: Arquivo inválido.']);
        }

        $results = Excel::load($request->arquivo)->formatDates(false)->get();
        $erros   = [];

        foreach ($results as $rowNum => $row) {
            $rowNum = $rowNum+2;

            if (Tools::isEmptyRow($row)) continue;

            if ($row->mes) {
                if (! preg_match('/^([1-9]|0[1-9]|1[012])$/', $row->mes)) {
                    $erros[] = 'Linha '. $rowNum .' - Mês inválido.';
                }
            }
            if ($row->ano) {
                if (! preg_match('/^\d{4}$/', $row->ano)) {
                    $erros[] = 'Linha '. $rowNum .' - Ano inválido.';
                }
            }
            if (! $row->valor) {
                $erros[] = 'Linha '. $rowNum .' - Valor não preenchido.';
            } elseif (preg_match('/[^\d,.]/', $row->valor)) {
                $erros[] = 'Linha '. $rowNum .' - Valor inválido.';
            }
        }

        if (count($erros)) {
            return back()->withErrors(
                array_merge(['Erro ao importar arquivo:'],$erros
            ));
        } else {
            $fundo->grafico()->delete();

            foreach ($results as $row) {
                if (Tools::isEmptyRow($row)) continue;

                $mes = intval($row->mes);
                $ano = intval($row->ano);

                $fundo->grafico()->create([
                    'mes'   => $mes && $ano ? $mes : '',
                    'ano'   => $mes && $ano ? $ano : '',
                    'valor' => $row->valor
                ]);
            }

            return back()->with('success', 'Arquivo importado com sucesso.');
        }
    }

    public function limpar(Fundo $fundo)
    {
        $fundo->grafico()->delete();

        return redirect()->route('painel.fundos.grafico.index', $fundo)->with('success', 'Gráfico excluído com sucesso.');
    }

    public function modelo(Fundo $fundo)
    {
        Excel::create('beowulf_modelo-grafico', function ($excel) {
            $excel->sheet('grafico', function ($sheet) {
                $sheet->fromArray(['mes', 'ano', 'valor']);
            });
        })->download('xls');
    }
}
